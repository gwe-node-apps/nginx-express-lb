var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/api/ping", function (req, res, next) {
  res.status(200).json({
    time: new Date().toUTCString(),
    name: process.env.NAME || "ExpressApp",
    port: process.env.PORT,
  });
});

module.exports = router;
