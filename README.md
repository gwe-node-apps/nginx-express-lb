### A simple Expressjs App to demonstrate NGINX Load balancing

[Reference](https://www.digitalocean.com/community/tutorials/how-to-set-up-nginx-load-balancing)   
[Reference 2](https://blog.jscrambler.com/scaling-node-js-socket-server-with-nginx-and-redis/)   
[Reference 3](https://upcloud.com/community/tutorials/configure-load-balancing-nginx/)    

0. start the ExpressJS with two different port numbers as
   ```
      > PORT=6100 NAME=app-01 npm run start -d
      > PORT=6200 NAME=app-02 npm run start -d
   ```
1. create a conf file (nginx-lb.conf) under /nginx/conf.d and write the following config and save

   ```
   upstream nodes {
     #ip_hash;
     server 127.0.0.1:6100;
     server 127.0.0.1:6200;
   }
   server {
     listen 3000;
     location / {
        proxy_pass http://nodes;
     }
   }
   ```
2. Restart NGINX

   ```
   sudo nginx -s stop
   sudo nginx
   ```
3. Access as

   ```
   curl http://localhost:3000/api/ping
   ```
